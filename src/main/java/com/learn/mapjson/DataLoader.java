package com.learn.mapjson;

import com.learn.mapjson.entity.User;
import com.learn.mapjson.entity.UserProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

// Component for loading Dummy data into database
@Component
public class DataLoader implements CommandLineRunner {

    private UserRepository userRepository;

    public DataLoader(@Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        UserProperties properties = new UserProperties();
        properties.setBlacklisted(false);
        properties.setPremiumUser(true);

        User dummyUser = new User();
        dummyUser.setProperties(properties);

        userRepository.save(dummyUser);
    }
}

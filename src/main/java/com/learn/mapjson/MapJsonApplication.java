package com.learn.mapjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapJsonApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapJsonApplication.class, args);
    }

}

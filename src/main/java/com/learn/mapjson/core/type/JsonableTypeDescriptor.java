package com.learn.mapjson.core.type;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.type.SerializationException;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Collection;

public class JsonableTypeDescriptor<T extends Serializable> extends AbstractTypeDescriptor<T> {
	private static final long serialVersionUID = -8332945841797908655L;

	private static final ObjectMapper MAPPER;
	static {
		MAPPER = new ObjectMapper();
		MAPPER.registerModule(new JSR310Module());
		MAPPER.registerModule(new JavaTimeModule());
	}

	private final JavaType javaType;

	protected JsonableTypeDescriptor(Class<T> type, Type genericType) {
		super(type);
//		super(type, new SerializableMutabilityPlan.INSTANCE.);
		Preconditions.checkArgument(type != null, "Type cannot be null.");
		javaType = MAPPER.getTypeFactory().constructType(genericType);
		checkEqualsHashCode(type);
	}

	private void checkEqualsHashCode(Class<T> type) {
		try {
			type.getDeclaredMethod("equals", Object.class);
		} catch (NoSuchMethodException e) {
			Preconditions.checkState(false, "Every class which is used as a JsonableType must override equals and hashCode, but %s does not.", type.getCanonicalName());
		}
		try {
			type.getDeclaredMethod("hashCode");
		} catch (NoSuchMethodException e) {
			Preconditions.checkState(false, "Every class which is used as a JsonableType must override equals and hashCode, but %s does not.", type.getCanonicalName());
		}
	}

	@Override
	public String toString(T value) {
		try {
			if (value == null) {
				return null;
			}
			if (javaType.isCollectionLikeType() && !(value instanceof Collection)) {
				// Necessary to use JsonableType in a Criteria query.
				JavaType elementJavaType = MAPPER.getTypeFactory().constructType(javaType.getContentType());
				return MAPPER.writerWithType(elementJavaType).writeValueAsString(value);
			}
			return MAPPER.writerWithType(javaType).writeValueAsString(value);
		} catch (IOException e) {
			throw new SerializationException("Could not serialize value", e);
		}
	}

	@Override
	public T fromString(String string) {
		try {
			if (StringUtils.isBlank(string)) {
				return null;
			}
			return MAPPER.readValue(string, javaType);
		} catch (IOException e) {
			throw new SerializationException("Could not deserialize value", e);
		}
	}

	public byte[] toBytes(T value) {
		try {
			return MAPPER.writerWithType(javaType).writeValueAsBytes(value);
		} catch (IOException e) {
			throw new SerializationException("Could not serialize value", e);
		}
	}

	public T fromBytes(byte[] bytes) {
		try {
			return MAPPER.readValue(bytes, javaType);
		} catch (IOException e) {
			throw new SerializationException("Could not deserialize value", e);
		}
	}

	private T fromInputStream(InputStream stream) {
		try {
			return MAPPER.readValue(stream, javaType);
		} catch (IOException e) {
			throw new SerializationException("Could not deserialize value", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <X> X unwrap(T value, Class<X> type, WrapperOptions options) {
		if (value == null) {
			return null;
		} else if (String.class.isAssignableFrom(type)) {
			return (X) toString(value);
		} else if (byte[].class.isAssignableFrom(type)) {
			return (X) toBytes(value);
		} else if (InputStream.class.isAssignableFrom(type)) {
			return (X) new ByteArrayInputStream(toBytes(value));
		}
		throw unknownUnwrap(getJavaTypeClass());
	}

	@Override
	public <X> T wrap(X value, WrapperOptions options) {
		if (value == null) {
			return null;
		} else if (String.class.isInstance(value)) {
			return fromString((String) value);
		} else if (byte[].class.isInstance(value)) {
			return fromBytes((byte[]) value);
		} else if (InputStream.class.isInstance(value)) {
			return fromInputStream((InputStream) value);
		}
		throw unknownWrap(value.getClass());
	}
}

package com.learn.mapjson.core.type;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.sql.LongVarcharTypeDescriptor;
import org.hibernate.usertype.DynamicParameterizedType;
import org.springframework.util.ReflectionUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Properties;

public class JsonableType<T extends Serializable> extends AbstractSingleColumnStandardBasicType<T> implements DynamicParameterizedType {
	private static final long serialVersionUID = 6104499369257987002L;

	public static final String NAME = "com.learn.mapjson.core.type.JsonableType";

	public JsonableType() {
		// Java type descriptor is set with the parameter values.
		super(LongVarcharTypeDescriptor.INSTANCE, null);
	}

	@Override
	public String getName() {
		return NAME;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setParameterValues(Properties parameters) {
		try {
			// TODO: Currently only supports field level annotations. Enhance to support method level annotations.
			Class<?> entityClass = Class.forName(parameters.getProperty(DynamicParameterizedType.ENTITY));

			Field field = Optional.ofNullable(ReflectionUtils.findField(entityClass, parameters.getProperty(DynamicParameterizedType.PROPERTY)))
					.orElseThrow(NoSuchFieldException::new);

			Class<T> type = (Class<T>) field.getType();
			setJavaTypeDescriptor(new com.learn.mapjson.core.type.JsonableTypeDescriptor<>(type, field.getGenericType()));
		} catch (ClassNotFoundException | NoSuchFieldException e) {
			// Should not happen.
			throw new IllegalStateException(e);
		}
	}
}
